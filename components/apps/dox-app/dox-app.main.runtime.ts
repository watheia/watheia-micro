/** @format */

import { ApplicationAspect, ApplicationMain } from "@teambit/application"
import { MainRuntime } from "@teambit/cli"
import UIAspect, { UiMain } from "@teambit/ui"
import { ComponentAspect, ComponentMain } from "@teambit/component"
import { DoxAppOptions } from "./dox-app.options"
import { DoxAppAspect } from "./dox-app.aspect"
// import { HarmonyUIApp } from './ui.application';

export class DoxAppMain {
  constructor(
    private application: ApplicationMain,
    private ui: UiMain,
    private componentAspect: ComponentMain
  ) {}

  /**
   * register a new harmony UI application.
   */
  registerDoxApp(options: DoxAppOptions) {
    this.ui.registerUiRoot({
      name: options.name,
      path: this.componentAspect.getHost().path,
      configFile: "",
      async resolvePattern() {
        return []
      },
      async resolveAspects() {
        return options.aspectDefs
      }
    })

    // this.application.registerApp(new HarmonyUIApp(options.name, this.ui));

    return this
  }

  static slots = []

  static dependencies = [ApplicationAspect, UIAspect, ComponentAspect]

  static runtime = MainRuntime

  static async provider([application, ui, componentAspect]: [
    ApplicationMain,
    UiMain,
    ComponentMain
  ]) {
    return new DoxAppMain(application, ui, componentAspect)
  }
}

DoxAppAspect.addRuntime(DoxAppMain)
