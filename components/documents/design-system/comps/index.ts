/** @format */

export { default as ColorsComp } from "./WatheiaColors.svg"
export { default as ElevationComp } from "./WatheiaElevation.svg"
export { default as StatesComp } from "./WatheiaStates.svg"
export { default as ThemeComp } from "./WatheiaTheme.svg"
export { default as TypographyComp } from "./WatheiaColors.svg"
