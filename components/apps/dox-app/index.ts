/** @format */

import { DoxAppAspect } from "./dox-app.aspect"

export type { DoxAppOptions } from "./dox-app.options"
export type { DoxAppMain } from "./dox-app.main.runtime"
export default DoxAppAspect
export { DoxAppAspect }
