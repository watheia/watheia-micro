/** @format */

import React from "react"
import { render } from "@testing-library/react"
import { BasicLayout } from "./layout.composition"

it("should render with the correct text", () => {
  const { getAllByTestId } = render(
    <div data-testid="basic-layout">
      <BasicLayout />
    </div>
  )
  const rendered = getAllByTestId("basic-layout")
  expect(rendered).toBeTruthy()
})
