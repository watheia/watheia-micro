/** @format */

import { AspectDefinition } from "@teambit/aspect-loader"

export type DoxAppOptions = {
  /**
   * name of the app. e.g. 'ripple-ci'
   */
  name: string

  aspectDefs: AspectDefinition[]
}
