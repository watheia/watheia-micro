# This file is a template, and might need editing before it works on your project.
FROM node:lts-alpine

#  use of `process.dlopen` is necessary
RUN apk add --no-cache libc6-compat \
  bash \
  gcc \
  git \
  git-lfs \
  make \
  python \
  g++

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

RUN yarn global add @teambit/bvm --unsafe-perm=true

USER node
ENV PATH "$PATH:$HOME/bin"
WORKDIR /home/node/app
COPY --chown=node . .

RUN bvm install && \
  /home/node/bin/bit init --harmony && \
  /home/node/bin/bit config set analytics_reporting false && \
  /home/node/bin/bit config set error_reporting false && \
  /home/node/bin/bit config set no_warnings true && \
  /home/node/bin/bit install && \
  /home/node/bin/bit environment

CMD ["/home/node/bin/bit", "start"]
