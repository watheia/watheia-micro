/** @format */

import { Aspect } from "@teambit/harmony"

export const DoxAppAspect = Aspect.create({
  id: "watheia.dox/dox-app"
})
