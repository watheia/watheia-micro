/** @format */

import React from "react"
import { Layout } from "./layout"

export const BasicLayout = () => (
  <Layout title="hello from Layout" canonical="https://bit.dev/watheia/dox/ui/layout">
    {" "}
    <p>Hello, World!</p>
  </Layout>
)
