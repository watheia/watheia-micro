/** @format */

import React from "react"
import Head from "next/head"

import "./globals.css"

export type LayoutProps = {
  /**
   * The page title to set in the document head.
   */
  title?: string
  canonical?: string
  description?: string
} & React.HTMLAttributes<HTMLDivElement>

const defaultProps = {
  title: "Micro Frontends by Watheia Labs",
  description:
    "Watheia Labs, LLC is an engineering and digital design firm located in Southeast Washington State",
}

export function Layout({
  title,
  description,
  canonical,
  children,
  ...props
}: LayoutProps = defaultProps) {
  return (
    <div {...props}>
      <Head>
        <meta charSet="utf-8" />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="https://cdn.watheia.org/compendium/v1/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="https://cdn.watheia.org/compendium/v1/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="https://cdn.watheia.org/compendium/v1/favicon-16x16.png"
        />
        <link
          rel="manifest"
          href="https://cdn.watheia.org/compendium/v1/site.webmanifest"
        />
        <meta name="theme-color" content="#092947" />
        {description && <meta name="description" content={description} />}
        <meta
          name="robots"
          content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"
        />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:image"
          content="https://cdn.watheia.org/compendium/v1/icons/icon@4x.png"
        />
        {title && <meta property="og:title" content={title} />}
        {title && <title>{title}</title>}
        {description && <meta property="og:description" content={description} />}
        {canonical && <meta property="og:url" content={canonical} />}
        {canonical && <link rel="canonical" href={canonical}></link>}
      </Head>
      {children}
    </div>
  )
}
